# house-booking-api

House Booking API

#### Docker build image

```bash
docker build -t [IMAGE_API] .
```

## Running container

```
When you want to modify the code using the docker flow run the container with this command
```

docker run -d \
-p 8000:8000 \
--restart unless-stopped \
-v $(pwd):/usr/src/app \
--name [CONTAINER_API] \
[IMAGE_API] npm run start:dev