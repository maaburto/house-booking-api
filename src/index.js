/* eslint-disable no-console */
import http from 'http';
import cors from 'cors';
import express from 'express';
import { ApolloServer } from 'apollo-server-express';

import { rootGraphql, port } from './config/constant';
import Models from './db/models';
import resolvers from './graphql/resolvers';
import schemas from './graphql/schemas';

const app = express();
app.use(cors());

const me = Models.users[0];

const apolloOpts = {
  typeDefs: schemas,
  resolvers,
  context: { models: Models, me },
};
const server = new ApolloServer(apolloOpts);

server.applyMiddleware({ app, path: `/${rootGraphql}` });
const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen({ port }, () => {
  console.log(`🚀 Apollo Server on http://localhost:${port}/${rootGraphql}`);
});
