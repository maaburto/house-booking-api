import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    baths: [Bath!]!
    bath(id: ID!): Bath
  }

  type Bath {
    id: ID!
    name: String!
    description: String!
  }
`;
