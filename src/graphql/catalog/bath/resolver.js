export default {
  Query: {
    baths: (_, args, { models }) => models.bath,
    bath: (_, { id }, { models }) => models.bath.find(
      ({
        id: bathId,
      }) => bathId === parseInt(id, 10),
    ),
  },
};
