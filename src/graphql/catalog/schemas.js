import bed from './bed/schema';
import bedRoom from './bedRoom/schema';
import bath from './bath/schema';
import service from './service/schema';

export default [bed, bath, bedRoom, service];
