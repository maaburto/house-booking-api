import service from './service/resolver';
import bed from './bed/resolver';
import bedRoom from './bedRoom/resolver';
import bath from './bath/resolver';

export default {
  bed, bedRoom, bath, service,
};
