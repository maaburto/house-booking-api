export default {
  Query: {
    services: (_, args, { models }) => models.service,
    service: (_, { id }, { models }) => models.service.find(
      ({
        id: serviceId,
      }) => serviceId === parseInt(id, 10),
    ),
  },

  Service: {
    parentId: (service, args, { models }) => models.service.find(
      ({
        id,
      }) => service.parentId === id,
    ),
  },
};
