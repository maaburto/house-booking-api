import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    services: [Service!]!
    service(id: ID!): Service
  }

  type Service {
    id: ID!
    title: String!
    description: String!
    parentId: Service
  }
`;
