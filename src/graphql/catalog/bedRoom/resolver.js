export default {
  Query: {
    bedRooms: (_, args, { models }) => models.bedRoom,
    bedRoom: (_, { id }, { models }) => models.bedRoom.find(
      ({
        id: bedRoomId,
      }) => bedRoomId === parseInt(id, 10),
    ),
  },
};
