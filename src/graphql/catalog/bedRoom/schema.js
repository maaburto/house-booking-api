import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    bedRooms: [BedRoom!]!
    bedRoom(id: ID!): BedRoom
  }

  type BedRoom {
    id: ID!
    name: String!
    description: String!
  }
`;
