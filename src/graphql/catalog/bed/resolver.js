export default {
  Query: {
    beds: (_, args, { models }) => models.bed,
    bed: (_, { id }, { models }) => models.bed.find(
      ({
        id: bedId,
      }) => bedId === parseInt(id, 10),
    ),
  },
};
