import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    beds: [Bed!]!
    bed(id: ID!): Bed
  }

  type Bed {
    id: ID!
    name: String!
    description: String!
  }
`;
