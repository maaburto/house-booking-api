import { gql } from 'apollo-server-express';

import user from './user/schema';
import message from './message/schema.message';
import host from './host/schema';
import house from './house/main/schema';
import houseInformation from './house/information/schema';
import houseDescription from './house/description/schema';
import houseBath from './house/bath/schema';
import houseBedRoom from './house/bedRoom/schema';
import houseBed from './house/bed/schema';
import houseService from './house/service/schema';
import catalog from './catalog/schemas';

const link = gql`
scalar JSON

type Query {
  _: Boolean
}
type Mutation {
  _: Boolean
}
type Subscription {
  _: Boolean
}
`;

export default [
  ...catalog,
  link,
  user,
  message,
  host,
  house,
  houseInformation,
  houseDescription,
  houseBath,
  houseBedRoom,
  houseBed,
  houseService,
];
