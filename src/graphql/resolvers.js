import GraphQLJSON from 'graphql-type-json';

import user from './user/resolver';
import message from './message/resolver.message';
import host from './host/resolver';
import house from './house/main/resolver';
import houseInformation from './house/information/resolver';
import houseBed from './house/bed/resolver';
import houseBedRoom from './house/bedRoom/resolver';
import houseBath from './house/bath/resolver';
import houseService from './house/service/resolver';
import catalog from './catalog/resolvers';

const {
  bed, service, bedRoom, bath,
} = catalog;

const scalar = {
  JSON: GraphQLJSON,
};

export default [
  user,
  message,
  host,
  house,
  houseInformation,
  houseBed,
  houseBedRoom,
  houseBath,
  service,
  bed,
  bedRoom,
  bath,
  houseService,
  scalar,
];
