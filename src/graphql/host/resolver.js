export default {
  Query: {
    host: (_, { id }, { models }) => models.hosts.find(
      ({ id: hostId }) => hostId === parseInt(id, 10),
    ),
    hosts: (_, args, { models }) => models.hosts,
  },

  Host: {
    user: (host, args, { models }) => models.users.find(({ id }) => host.userId === id),
  },
};
