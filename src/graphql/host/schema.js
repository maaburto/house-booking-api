import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    hosts: [Host!]
    host(id: ID!): Host
  }

  type Host {
    id: ID!
    user: User
    address: String!
    address2: String
    city: String!
    state: String!
    country: String!
    zip: String
  }
`;
