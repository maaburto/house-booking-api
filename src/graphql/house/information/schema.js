import { gql } from 'apollo-server-express';

export default gql`
  type HouseInformation {
    id: ID!
    title: String!
    numberGuest: Int!
    house: House
    houseBath: [HouseBath!]!
    houseBed: [HouseBed!]!
    houseBedRoom: [HouseBedRoom!]!
  }
`;
