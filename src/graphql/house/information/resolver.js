export default {
  HouseInformation: {
    houseBed: (houseInformation, args, { models }) => models.houseBed.filter(
      ({ houseInformationId }) => houseInformationId === houseInformation.id,
    ),

    houseBath: (houseInformation, args, { models }) => models.houseBath.filter(
      ({ houseInformationId }) => houseInformationId === houseInformation.id,
    ),

    houseBedRoom: (houseInformation, args, { models }) => models.houseBedRoom.filter(
      ({ houseInformationId }) => houseInformationId === houseInformation.id,
    ),
  },
};
