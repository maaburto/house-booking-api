import { gql } from 'apollo-server-express';

export default gql`
  type HouseBedRoom {
    id: ID!
    description: String
    bedRoom: BedRoom!
    houseInformation: HouseInformation!
  }
`;
