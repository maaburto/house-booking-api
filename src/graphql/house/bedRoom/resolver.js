export default {
  HouseBedRoom: {
    bedRoom: (houseBath, args, { models }) => models.bedRoom.find(
      ({ id: bedRoomId }) => houseBath.bedRoomId === bedRoomId,
    ),
  },
};
