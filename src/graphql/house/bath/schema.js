import { gql } from 'apollo-server-express';

export default gql`
  type HouseBath {
    id: ID!
    description: String
    bath: Bath!
    houseInformation: HouseInformation!
  }
`;
