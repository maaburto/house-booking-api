export default {
  HouseBath: {
    bath: (houseBath, args, { models }) => models.bath.find(
      ({ id: bathId }) => houseBath.bathId === bathId,
    ),
  },
};
