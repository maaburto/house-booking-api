export default {
  HouseBed: {
    bed: (houseBed, args, { models }) => models.bed.find(
      ({ id: bedId }) => houseBed.bedId === bedId,
    ),
  },
};
