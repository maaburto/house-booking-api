import { gql } from 'apollo-server-express';

export default gql`
  type HouseBed {
    id: ID!
    description: String
    bed: Bed!
    houseInformation: HouseInformation!
  }
`;
