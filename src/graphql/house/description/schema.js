import { gql } from 'apollo-server-express';

export default gql`
  type HouseDescription {
    id: ID!
    general: String!
    space: String!
    guestAccess: String
    note: String
    house: House
  }
`;
