export default {
  Query: {
    house: (_, { id }, { models }) => models.houses.find(
      ({
        id: houseId,
      }) => houseId === parseInt(id, 10),
    ),
    houses: (_, args, { models }) => models.houses,
  },

  House: {
    host: (house, args, { models }) => models.hosts.find(({ id }) => house.hostId === id),

    houseInformation: (house, args, { models }) => models.houseInformations.find(
      ({ houseId }) => house.id === houseId,
    ),

    houseDescription: (house, args, { models }) => models.houseDescription.find(
      ({ houseId }) => house.id === houseId,
    ),

    houseService: (house, args, { models }) => models.houseService.filter(
      ({ houseId }) => house.id === houseId,
    ),
  },
};
