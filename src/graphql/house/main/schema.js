import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    houses: [House!]
    house(id: ID!): House
  }

  type House {
    id: ID!
    host: Host
    title: String!
    checkIn: String!
    checkOut: String!
    lowSeasonRate: Float!
    highSeasonRate: Float!
    seasonRate: Float!
    images: [String]!
    houseInformation: HouseInformation
    houseDescription: HouseDescription
    houseService: [HouseService]!
  }
`;
