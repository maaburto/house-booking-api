import { gql } from 'apollo-server-express';

export default gql`
  type HouseService {
    id: ID!
    house: House!
    service: Service!
    description: String
  }
`;
