export default {
  HouseService: {
    service: (houseService, args, { models }) => models.service.find(
      ({ id: serviceId }) => serviceId === houseService.serviceId,
    ),
  },
};
