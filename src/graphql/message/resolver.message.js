import uuidv4 from 'uuid/v4';
import pubsub, { EVENTS } from '../../subscription';

export default {
  Query: {
    messages: (_, args, { models }) => models.messages,
    message: (_, { id }, { models }) => models.messages.find(
      ({
        id: messageId,
      }) => messageId === id,
    ),
  },

  Mutation: {
    // eslint-disable-next-line no-shadow
    createMessage: (_, { text }, { models, me }) => {
      const id = uuidv4();
      const message = {
        id,
        text,
        userId: me.id,
      };

      models.messages.push(message);

      pubsub.publish(EVENTS.MESSAGE.CREATED, {
        messageCreated: { message },
      });

      return message;
    },
    deleteMessage: (_, { id }, { models }) => {
      const { [id]: message, ...otherMessages } = models.messages;

      if (!message) {
        return false;
      }

      // eslint-disable-next-line no-console
      console.log(otherMessages);
      return true;
    },
  },

  Subscription: {
    messageCreated: {
      subscribe: () => pubsub.asyncIterator(EVENTS.MESSAGE.CREATED),
    },
  },

  Message: {
    user: (message, args, { models }) => models.users.find(({ id }) => message.userId === id),
  },
};
