import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    me: User
    users: [User!]
    user(id: ID!): User
  }

  type User {
    id: ID!
    username: String!
    password: String!
    firstName: String!
    lastName: String!
    middleName: String
    fullName: String!
    email: String!
    cellPhone: [String]!
    alternateEmail: String
    host: Host
    house: House
    messages: [Message!]
  }
`;
