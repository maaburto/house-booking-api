export default {
  Query: {
    me: (_, args, { me }) => me,
    user: (_, { id }, { models }) => models.users.find(
      ({ id: userId }) => userId === parseInt(id, 10),
    ),
    users: (_, args, { models }) => models.users,

  },

  User: {
    fullName: ({ firstName, lastName, middleName }) => `${lastName} ${middleName}. ${firstName}`,
    host: (user, args, { models }) => models.hosts.find(({ userId }) => user.id === userId),
    house: (user, args, { models }) => {
      const host = models.hosts.find(({ userId }) => userId === user.id);
      return models.houses.find(({ hostId }) => hostId === host.id);
    },
    messages: (user, args, { models }) => models.messages.filter(
      (message) => message.userId === user.id,
    ),
  },
};
