export const hosts = [{
  id: 1,
  userId: 1,
  address: 'Del Belagio 123',
  address2: null,
  city: 'San Francisco',
  state: 'CA',
  country: 'USA',
  zip: '129082',
}, {
  id: 2,
  userId: 2,
  address: 'Address Fake Blvd 124',
  address2: null,
  city: 'Taladega bay',
  state: 'WY',
  country: 'USA',
  zip: '129332',
}];
