import * as users from './user';
import * as messages from './message';
import * as hosts from './host';
import * as houses from './house';
import * as houseInformations from './house/information';
import * as houseDescription from './house/description';
import * as houseBath from './house/bath';
import * as houseBed from './house/bed';
import * as houseBedRoom from './house/bedRoom';
import * as houseService from './house/service';
import * as catalog from './catalog';

export default {
  ...catalog,
  ...users,
  ...messages,
  ...houses,
  ...houseInformations,
  ...houseDescription,
  ...houseBath,
  ...houseBed,
  ...houseBedRoom,
  ...houseService,
  ...hosts,
};
