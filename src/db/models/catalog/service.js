export const service = [{
  id: 1,
  title: 'Basic',
  description: 'Basic Section',
  parentId: null,
}, {
  id: 2,
  title: 'Wifi',
  description: 'Wifi',
  parentId: 1,
}, {
  id: 3,
  title: 'TV',
  description: 'TV',
  parentId: 1,
}];
