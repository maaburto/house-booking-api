import { bath } from './bath';
import { bed } from './bed';
import { bedRoom } from './bedRoom';
import { service } from './service';

export {
  bath,
  bed,
  bedRoom,
  service,
};
