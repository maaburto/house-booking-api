const users = [{
  id: 1,
  username: 'rwieruch',
  password: '',
  firstName: 'Robin',
  lastName: 'Wieruch',
  middleName: 'A',
  email: 'rwieruch@example.com',
  cellPhone: ['922922818', '922922812'],
  alternateEmail: null,
}, {
  id: 2,
  username: 'ddavis',
  password: '',
  firstName: 'David',
  lastName: 'Davis',
  middleName: 'W',
  email: 'ddavis@example.com',
  cellPhone: ['922922818', '922922812'],
  alternateEmail: null,
}];

export { users };
