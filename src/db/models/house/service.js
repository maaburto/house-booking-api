export const houseService = [{
  id: 1,
  description: null,
  houseId: 1,
  serviceId: 1,
}, {
  id: 2,
  description: 'Wifi at whole house!',
  houseId: 1,
  serviceId: 2,
}, {
  id: 3,
  description: 'TV Led 32\'\'',
  houseId: 1,
  serviceId: 3,
}, {
  id: 4,
  description: null,
  houseId: 2,
  serviceId: 1,
}, {
  id: 5,
  description: 'Wifi only at bedroom',
  houseId: 2,
  serviceId: 2,
}, {
  id: 6,
  description: 'TV Led 24\'\'',
  houseId: 2,
  serviceId: 3,
}];
