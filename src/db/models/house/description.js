export const houseDescription = [{
  id: 1,
  general: 'Our 225 sq ft (11.5’x19.5’) lakeshore guesthouse is in front of our home, north of Duluth. 18 miles from Downtown, 20 miles from Canal Park (20-30 min drive). If you’re looking to stay right on the water of one of northern MN’s most sought-after lakes, then this tiny, rare, newly-renovated “boathouse” should be a fit :) 2 Restaurants/bars within walking distance & others 5-8 mi away (Boondocks reopened under new ownership)',
  space: 'This is a VERY SMALL, one-room “studio” guest house - newly renovated - with a Queen memory foam cabinet murphy bed, Queen slide-out sleeper couch, SmartTV & DVD player. It is almost directly over the water, & has its own tiny deck. It was originally a boat house from 1970, and can only be “maintained” as the size and place it is, because it is a “grandfathered” structure (incl. it’s deck); MN no longer allows structures within 30 ft of lakes.',
  guestAccess: 'Guests can use the Cottage/guest house, it\'s attached deck, the black carpeted dock, and yard adjacent. There is a sloping path into the lake also, which is “beach-grass” covered (near the fire pit)',
  note: 'Hiking is possible in the summer on nearby snowmobile or cross-country ski trails. Or into the woods on county land just one min. south of us (for example from the county gravel pit on the east side of Hwy 4',
  houseId: 1,
}];
