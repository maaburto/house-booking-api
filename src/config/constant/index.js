import 'dotenv/config';

export const mySecret = process.env.MY_SECRET;
export const port = 8000;
export const rootGraphql = 'graphql';
